package com.javaLive.exceptionhandling.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.javaLive.exceptionhandling.realtime.basic.InvalidProductException;

public class LoggingWithException {
	private static final Logger logger = LoggerFactory.getLogger(LoggingWithException.class); // SLF4J

	void productCheck(int weight) throws InvalidProductException {
		if (weight < 100) {
			throw new InvalidProductException("Product Invalid");
		}
	}

	public static void main(String args[]) {
		LoggingWithException obj = new LoggingWithException();
		try {
			obj.productCheck(60);
		} catch (InvalidProductException ex) {
			System.out.println("Caught the exception");
			//logger.error("\nRaised exception of type "+ex.getClass()+"\t"+ex.getMessage());//Logging the error message.
			logger.error(ex.getMessage(),ex);//VIMP method signature which not only describe the 
											//error message but also provide actual stack along with 
											//the line number in the file.
		}
	}

}
