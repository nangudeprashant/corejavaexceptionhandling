package com.javaLive.exceptionhandling.realtime.basic;

public class UserDefinedExceptionDemo
{
   void productCheck(int weight) throws InvalidProductException{
	if(weight<100){
		System.out.println("Throwing exception.........");
		throw new InvalidProductException("Invalid product.");
	}
   }
   
    public static void main(String args[])
    {
    	UserDefinedExceptionDemo obj = new UserDefinedExceptionDemo();
        try
        {
            obj.productCheck(60);
        }
        catch (InvalidProductException ex)
        {
            System.out.println("Caught the exception in calling method.");
            System.out.println("Printing message of user defined exception in catch block....");
            System.out.println(ex.getMessage());
        }
    }
}