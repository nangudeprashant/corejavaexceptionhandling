package com.javaLive.exceptionhandling.realtime.advance;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RealTimeExampleForCheckedExceptionIMP {
	private static final Logger logger = LoggerFactory.getLogger(RealTimeExampleForCheckedExceptionIMP.class); // SLF4J

	private void wrapException(String input) throws MyBusinessCheckedException {
		try {
			// do something
			// Integer.parseInt("po");
			throw new IOException();
		} catch (IOException e) {
			/*
			 * errorDescription: holds a description of the error with all the necessary
			 * details needed for users, application operators, and possibly the application
			 * developers, to understand what error occurred.
			 */
			throw new MyBusinessCheckedException("A message that describes the error.", e,
					ErrorCode.INVALID_PORT_CONFIGURATION);
		}
	}

	public void handleExceptionInOneBlock() {
		try {
			wrapException(new String("99999999"));
		} catch (MyBusinessCheckedException e) {
			// handle exception
			logger.error(e.getMessage() + "\n" + e.getCode().getDescription());
		}
	}

	public static void main(String[] args) {
		RealTimeExampleForCheckedExceptionIMP obj = new RealTimeExampleForCheckedExceptionIMP();
		obj.handleExceptionInOneBlock();
	}
}
