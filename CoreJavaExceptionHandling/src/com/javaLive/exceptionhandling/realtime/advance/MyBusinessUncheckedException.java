package com.javaLive.exceptionhandling.realtime.advance;

/**
 * The MyBusinessUncheckedException wraps all unchecked standard Java exception
 * and enriches them with a custom error code. You can use this code to retrieve
 * localized error messages and to link to our online documentation.
 * 
 * @author www.itaspirants.com
 */
class MyBusinessUncheckedException extends RuntimeException {
	//for more information about below declaration please go through file:
	// 'WhatIsSerialVersionUIDAndWhyDoWeNeedIt.docx' in this project.
	private static final long serialVersionUID = -8460356990632230194L;
	/*
	 * ErrorCode: Enumeration with a unique code which identifies this error, the
	 * errorCode tells what went wrong. This attribute indicates to the exception
	 * catching code what to do with the error.
	 */
	private final ErrorCode code;
	/*
	 * isSevere: indicates whether the error is severe or not, this attribute is
	 * updated as the exception traverse up the stack based on the context of the
	 * error, the severity indicates to the exception catching code whether to halt
	 * the application or keep processing.
	 */
	private boolean isSevere;

	public MyBusinessUncheckedException(ErrorCode code) {
		super();
		this.code = code;
	}

	public MyBusinessUncheckedException(String message, Throwable cause, ErrorCode code) {
		super(message, cause);
		this.code = code;
	}

	public MyBusinessUncheckedException(String message, ErrorCode code) {
		super(message);
		this.code = code;
	}

	public MyBusinessUncheckedException(String message, Throwable cause, ErrorCode code, boolean isSevere) {
		super(message, cause);
		this.code = code;
		this.isSevere = isSevere;
	}

	public MyBusinessUncheckedException(Throwable cause, ErrorCode code) {
		super(cause);
		this.code = code;
	}

	public ErrorCode getCode() {
		return this.code;
	}

	public boolean isSevere() {
		return isSevere;
	}

	public void setSevere(boolean isSevere) {
		this.isSevere = isSevere;
	}
}
