package com.javaLive.exceptionhandling.realtime.advance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RealTimeExampleForUncheckedExceptionIMP {
	private static final Logger logger = LoggerFactory.getLogger(RealTimeExampleForUncheckedExceptionIMP.class); // SLF4J
	private void wrapException(String input) {
		try {
			int num = Integer.parseInt(input);
		} catch (NumberFormatException e) {
			throw new MyBusinessUncheckedException("A message that describes the error.", e,
					ErrorCode.INVALID_NUMBER_INPUT);
		}
	}

	public void handleExceptionInOneBlock() {
		try {
			wrapException(new String("abc"));
		} catch (MyBusinessUncheckedException e) {
			// handle exception
			logger.error(e.getMessage() + "\n" + e.getCode().getDescription());
		}
	}

	public static void main(String[] args) {
		RealTimeExampleForUncheckedExceptionIMP obj = new RealTimeExampleForUncheckedExceptionIMP();
		obj.handleExceptionInOneBlock();
	}
}
// We can further extend this program by catching the
// MyBusinessUncheckedException in main method
// and there by logging proper message in log file as in case of
// MyBusinessException.