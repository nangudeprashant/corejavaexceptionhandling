package com.javaLive.exceptionhandling.realtime.advance;


/**
 * The MyBusinessException wraps all checked standard Java exception and
 * enriches them with a custom error code. 
 * 
 * @author www.itaspirants.com
 */
/*
 * Creating a custom checked exception is simple.
 * 
 * When you implement a checked exception, you need to extend the class
 * Exception. That�s the only thing you need to do to create a custom exception
 * class. But you should also provide a constructor method that sets the causing
 * exception and provide a benefit compared to the available standard
 * exceptions.
 * 
 * The following example does all of that. As you can see, we have added a
 * Javadoc comment that describes the exception and we also implemented a
 * constructor method that sets the causing exception on the superclass. And to
 * provide a benefit compared to the standard exceptions, MyBusinessException
 * uses a custom enumeration to store an error code that identifies the problem.
 * Clients can use the error code to show localized error messages or tell the
 * user to include this code in a support ticket.
 */
public class MyBusinessCheckedException extends Exception {
	private static final long serialVersionUID = 7718828512143293558L;
	//for more information about above declaration please go through file:
	// 'WhatIsSerialVersionUIDAndWhyDoWeNeedIt.docx' in this project.
	/*
	 * ErrorCode: Enumarion with a unique code which identifies this error, the
	 * errorCode tells what went wrong. This attribute indicates to the exception
	 * catching code what to do with the error.
	 */
	private final ErrorCode code;
	/*
	 * isSevere: indicates whether the error is severe or not, this attribute is
	 * updated as the exception traverse up the stack based on the context of the
	 * error, the severity indicates to the exception catching code whether to halt
	 * the application or keep processing.
	 */
	private boolean isSevere;

	public MyBusinessCheckedException(ErrorCode code) {
		super();
		this.code = code;
	}

	public MyBusinessCheckedException(String message, Throwable cause, ErrorCode code) {
		super(message, cause);
		this.code = code;
	}

	public MyBusinessCheckedException(String message, Throwable cause, ErrorCode code, boolean isSevere) {
		super(message, cause);
		this.code = code;
		this.isSevere = isSevere;
	}

	public MyBusinessCheckedException(String message, ErrorCode code) {
		super(message);
		this.code = code;
	}

	public MyBusinessCheckedException(Throwable cause, ErrorCode code) {
		super(cause);
		this.code = code;
	}

	public ErrorCode getCode() {
		return this.code;
	}

	public boolean isSevere() {
		return isSevere;
	}

	public void setSevere(boolean isSevere) {
		this.isSevere = isSevere;
	}
}
